import { Component, Input, OnInit } from '@angular/core';
import { Article } from 'src/app/interfaces/interfaces';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { ActionSheetController } from '@ionic/angular';
import { DataLocalService } from 'src/app/services/data-local.service';

@Component({
  selector: 'app-noticia',
  templateUrl: './noticia.component.html',
  styleUrls: ['./noticia.component.scss'],
})
export class NoticiaComponent implements OnInit {
  @Input() noticia: Article;
  @Input() indice: number;
  @Input() enFavoritos;

  constructor(
    private iab: InAppBrowser,
    private socialSharing: SocialSharing,
    private actionSheetController: ActionSheetController,
    private datalocalService: DataLocalService
  ) {}

  ngOnInit() {
    console.log('Favoritos', this.enFavoritos);
  }

  abrirNoticia() {
    const browser = this.iab.create(this.noticia.url, '_system');
  }
  async abrirMenu() {
    let guardarBorrarBtn;

    if (this.enFavoritos) {
      guardarBorrarBtn = {
        text: 'Quitar de Favoritos',
        icon: 'trash',
        handler: () => {
          console.log('Borrar de favoritos');
          this.datalocalService.borrarNoticia(this.noticia);
        },
      };
    } else {
      guardarBorrarBtn = {
        text: 'Agregar a Favoritos',
        icon: 'star',
        handler: () => {
          console.log('Se ha agregado a favoritos');
          this.datalocalService.guardarNoticia(this.noticia);
        },
      };
    }

    const actionSheet = await this.actionSheetController.create({
      buttons: [
        {
          text: 'Compartir',
          icon: 'share',
          handler: () => {
            console.log('Share clicked');
            this.socialSharing.share(
              this.noticia.title,
              this.noticia.source.name,
              this.noticia.url
            );
          },
        },
        guardarBorrarBtn,
        {
          text: 'Cancelar',
          icon: 'close',
          role: 'cancel',
          handler: () => {
            console.log('Usted ha cancelado esta acción');
          },
        },
      ],
    });
    await actionSheet.present();
  }
}
