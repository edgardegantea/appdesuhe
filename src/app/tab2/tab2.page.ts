import { ViewChild } from '@angular/core';
import { OnInit } from '@angular/core';
import { Component } from '@angular/core';
import { IonSegment } from '@ionic/angular';
import { Article } from '../interfaces/interfaces';
import { NoticiasService } from '../services/noticias.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit {

  @ViewChild(IonSegment) segment: IonSegment;
  
  noticias: Article[] = [];

  constructor(private noticiasService: NoticiasService) {}

  categorias = [
    'business','entertainment','general','health','science','sports','technology',
  ];

  ngOnInit(){
    this.segment.value=this.categorias[0];
    this.cargarNoticias(this.categorias[0]);
  }

  cargarNoticias(categoria: string){
    this.noticiasService.getTopHeadLinesCategory(categoria).subscribe(resp => {
      this.noticias.push(...resp.articles);
    });
  }

  cambiarCategoria(event){
    this.noticias = [];
    this.cargarNoticias(event.detail.value);
  }
}
