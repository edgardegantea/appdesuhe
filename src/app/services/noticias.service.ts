import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RespuestaTopHeadLines } from '../interfaces/interfaces';
import { environment } from 'src/environments/environment';

const apiKey = environment.apikey;
const apiUrl = environment.apiUrl;
const headers = new HttpHeaders({ 'X-Api-Key': apiKey });

@Injectable({
  providedIn: 'root',
})
export class NoticiasService {
  headlinesPage = 0;
  categoriaActual = '';
  categoriaPage = 0;

  constructor(private http: HttpClient) {}

  private consultarNoticias<T>(query: string) {
    query = apiUrl + query;
    return this.http.get<T>(query, { headers });
  }
  getTopHeadLines() {
    this.headlinesPage++;
    return this.consultarNoticias<RespuestaTopHeadLines>(
      `/top-headlines?country=mx&page=${this.headlinesPage}`
    );
  }

  getTopHeadLinesCategory(categoria: string) {
    if (this.categoriaActual === categoria) {
      this.categoriaPage++;
    } else {
      this.categoriaPage = 1;
      this.categoriaActual = categoria;
    }
    return this.consultarNoticias<RespuestaTopHeadLines>(
      `/top-headlines?country=mx&category=${categoria}&page=${this.categoriaPage}`
    );
  }
}
